sudoku book generator
=====================
sudokubook is command-line program written in c that generates printable classical sudoku puzzles (3x3) with exactly [17 clues](https://en.wikipedia.org/wiki/Mathematics_of_Sudoku#Minimum_number_of_givens) in HTML format. each page contains 6 puzzles arranged in a 2x3 grid format.

installing the program
----------------------

1. clone the repository:
- open your terminal and run the following command:
```sh
$ git clone https://codeberg.org/naf/sudokubook.git
```
2. navigate to the project directory:
- change your current directory to the newly cloned repository:
```sh
$ cd sudokubook
```

3. build and install the program:
- run this command as root to install the repository. (sudokubook is installed into the /usr/local/bin namespace by default)
```sh
# make clean install
```
    
running the program
-------------------
    $ sudokubook <number of pages>.

if no number of pages is specified, the default number of pages is 1.

screenshot
----------
![Image Alt text](/screenshot.png)

license
-------
this program is licensed under the MIT license. see the license file for details.

attribution
-----------
the sudoku puzzles generated are sourced from the collection by Gordon Royle and the University of Western Australia, licensed under a Creative Commons Attribution 2.5 License.

contact
-------
for any inquiries, please feel free to contact naf@disroot.org.


enjoy creating sudoku puzzle books! 🎉
