#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "puzzles.h"

#define LINE_LENGTH 90
#define LINE_COUNT 49158

int
getrandom (int min, int max, int exclude) {
	int random;
	do {
		random = min + rand() % (max - min + 1);
	} while (random == exclude);
	return random;
}

void
getgrid (char grid[][9]) {
	int index = getrandom(0, LINE_COUNT, -1);
	
	char puzzle[81];
	int i = 0, j = 0;
	while (puzzles[index][i] >= 48 && puzzles[index][i] <= 116) {
		if (puzzles[index][i] >= 48 && puzzles[index][i] <= 57) {
			puzzle[j++] = puzzles[index][i];
		} else if (puzzles[index][i] >= 102 && puzzles[index][i] <= 116) {
			for ( int x = puzzles[index][i] - 100; x > 0; x--) {
				puzzle[j++] = '0';
			}
		}
		i++;
	}

	int k = 0;
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			grid[i][j] = puzzle[k++];
		}
	}
}

void
reversegrid(char grid[][9]) {
	if(getrandom(0,1,0)) {
		int temp[9][9];
		for (int i = 0; i < 9 ; i++) { 
			for (int j = 0; j < 9; j++) {
				temp[8-i][8-j] = grid[i][j];
			}
		}
		for (int i = 0; i < 9 ; i++) { 
			for (int j = 0; j < 9; j++) {
				grid[i][j] = temp[i][j];
			}
		}
	}
}

void
shufflenumbers (char grid[][9]) {
	for (int i = 0; i < 9; i++) {
		int firstnumber = getrandom(1,9,0);
		int secondnumber = getrandom(1,9,firstnumber);
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (grid[i][j] == firstnumber) {
					grid[i][j] = secondnumber;
				} else if (grid[i][j] == secondnumber) {
					grid[i][j] = firstnumber;
				}
			}
		}
	}
}

void
shufflerows (char grid[][9]) {
	for (int k = 0; k < 9; k += 3) {
		int firstrow = getrandom(k,k+2,9);
		int secondrow = getrandom(3*(firstrow/3), 3*(firstrow/3)+2,firstrow);
		char temp;
		for ( int i = 0; i < 9; i++) {
			temp = grid[firstrow][i];
			grid[firstrow][i] = grid[secondrow][i];
			grid[secondrow][i] = temp;
		}
	}
}

void
shufflecolumns (char grid[][9]) {
	for (int k = 0; k < 9; k += 3) {
		int firstcolumn = getrandom(k,k+2,9);
		int secondcolumn = getrandom(3*(firstcolumn/3), 3*(firstcolumn/3)+2,firstcolumn);
		char temp;
		for ( int i = 0; i < 9; i++) {
			temp = grid[i][firstcolumn];
			grid[i][firstcolumn] = grid[i][secondcolumn];
			grid[i][secondcolumn] = temp;
		}
	}
}

void
shufflehorizontalbands (char grid[][9]) {
	for (int k = 0; k < 2; k++) {
		int firstband = getrandom(0,2,3);
		int secondband = getrandom(0,2,firstband);
		char temp;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				temp = grid[firstband*3+i][j];
				grid[firstband*3+i][j] = grid[secondband*3+i][j];
				grid[secondband*3+i][j] = temp;
			}
		}
	}
}

void
shuffleverticalbands (char grid[][9]) {
	for (int k = 0; k < 2; k++) {
		int firstband = getrandom(0,2,3);
		int secondband = getrandom(0,2,firstband);
		char temp;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				temp = grid[j][firstband*3+i];
				grid[j][firstband*3+i] = grid[j][secondband*3+i];
				grid[j][secondband*3+i] = temp;
			}
		}
	}
}

void
writehead (FILE *htmlfile) {
	fprintf(htmlfile,"<!DOCTYPE html>\n");
	fprintf(htmlfile,"<html lang='en'>\n");
	fprintf(htmlfile,"<head>\n");
	fprintf(htmlfile,"<meta charset='UTF-8'>\n");
	fprintf(htmlfile,"<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n");
	fprintf(htmlfile,"<title>Sudoku Book</title>\n");
	fprintf(htmlfile,"<style>\n");
	fprintf(htmlfile,"body { font-family: Arial, sans-serif; margin: 0; padding: 0; }\n");
	fprintf(htmlfile,"#page { width: 670px; height: 1000px; padding: 0;margin: auto; page-break-after: always; display: flex; flex-wrap: wrap; justify-content: space-between; }\n");
	fprintf(htmlfile,"table { margin: auto; box-sizing: border-box; overflow: hidden; display: inline-block; border: 3px solid #000000; }\n");
	fprintf(htmlfile,"td { border: 1px solid #000000; text-align: center; vertical-align: middle; border-style: dotted; }\n");
	fprintf(htmlfile,"input { color: #999999; padding: 0; border: 0; text-align: center; width: 28px; height: 28px; font-size: 20px; background-color: #FFFFFF; outline: none;}\n");
	fprintf(htmlfile,"input:disabled { color: #000000; background-color: #EEEEEE; font-size: 27px;}\n");
	fprintf(htmlfile,"#cell-0, #cell-1, #cell-2, #cell-3, #cell-4, #cell-5, #cell-6, #cell-7, #cell-8, #cell-27, #cell-28, #cell-29, #cell-30, #cell-31, #cell-32, #cell-33, #cell-34, #cell-35,#cell-54, #cell-55, #cell-56, #cell-57, #cell-58, #cell-59, #cell-60, #cell-61, #cell-62 { border-top: 2px solid #000000; }\n");
	fprintf(htmlfile,"#cell-18, #cell-19, #cell-20, #cell-21, #cell-22, #cell-23, #cell-24, #cell-25, #cell-26, #cell-45, #cell-46, #cell-47, #cell-48, #cell-49, #cell-50, #cell-51, #cell-52, #cell-53, #cell-72, #cell-73, #cell-74, #cell-75, #cell-76, #cell-77, #cell-78, #cell-79, #cell-80 { border-bottom: 2px solid #000000; }\n");
	fprintf(htmlfile,"#cell-0, #cell-9, #cell-18, #cell-27, #cell-36, #cell-45, #cell-54, #cell-63, #cell-72, #cell-3, #cell-12, #cell-21, #cell-30, #cell-39, #cell-48, #cell-57, #cell-66, #cell-75, #cell-6, #cell-15, #cell-24, #cell-33, #cell-42, #cell-51, #cell-60, #cell-69, #cell-78 { border-left: 2px solid #000000; }\n");
	fprintf(htmlfile,"#cell-2, #cell-11, #cell-20, #cell-29, #cell-38, #cell-47, #cell-56, #cell-65, #cell-74, #cell-5, #cell-14, #cell-23, #cell-32, #cell-41, #cell-50, #cell-59, #cell-68, #cell-77, #cell-8, #cell-17, #cell-26, #cell-35, #cell-44, #cell-53, #cell-62, #cell-71, #cell-80 { border-right: 2px solid #000000; }\n");
	fprintf(htmlfile,"@media print { header { display: none; } }\n");
	fprintf(htmlfile,"</style>\n");
	fprintf(htmlfile,"</head>\n");
}

void
writebody (FILE *htmlfile, int pagecount) {
	
	char grid[9][9];
	time_t currenttime = time(NULL);
	struct tm *timeinfo = gmtime(&currenttime);

	fprintf(htmlfile,"<body>\n");

	fprintf(htmlfile,"\t<header>\n");
	fprintf(htmlfile,"\t\tgenerated using <a href='https://codeberg.org/naf/sudokubook'>sudokubook</a>.<br>\n");
	fprintf(htmlfile,"\t\tall puzzles have exactly <a href='https://en.wikipedia.org/wiki/Mathematics_of_Sudoku#Minimum_number_of_givens'>17 clues</a>.<br>\n");
	fprintf(htmlfile, "\t\ttimestamp: %04d-%02d-%02d %02d:%02d:%02d utc.<br>\n", 
        timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday,
        timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	
	fprintf(htmlfile,"\t\tthis text is not printed. only puzzles are.<br>\n");
	fprintf(htmlfile,"\t</header>\n");

	for (int k = 0; k < 6 * pagecount; k++) {
		
		getgrid(grid);
		
		reversegrid(grid);
		shufflenumbers(grid);
		shufflehorizontalbands(grid);
		shuffleverticalbands(grid);
		shufflerows(grid);
		shufflecolumns(grid);
		
		if ( (k+6) % 6 == 0 )
			fprintf (htmlfile, "\t<div id='page'>\n");

		fprintf (htmlfile, "\t\t<table>\n");

		for ( int i = 0; i < 9; i++) {
			fprintf(htmlfile,"\t\t\t<tr>\n");
			for ( int j = 0; j < 9; j++) {
				if (grid[i][j] != '0') {
					fprintf (htmlfile, "\t\t\t\t<td><input id='cell-%d' type='text' value='%c' disabled></td>\n" , j+9*i, grid[i][j]);
				} else {
					fprintf (htmlfile, "\t\t\t\t<td><input id='cell-%d' type='text'></td>\n" , j+9*i);
				}
			}
			fprintf (htmlfile, "\t\t\t</tr>\n");
		}

		fprintf (htmlfile, "\t\t</table>\n");
		
		if ( (k+1) % 6 == 0 )
			fprintf (htmlfile, "\t</div>\n");
	}
	fprintf (htmlfile, "\t</body>\n</html>\n");
}

int
main (int argc, char *argv[]) {
	
	int pagecount;
	
	if (argc == 1) {
		pagecount = 1;
	} else if (argc == 2) {
		pagecount = atoi(argv[1]);
	} else {
		perror("usage: %s <number of pages>\n");
		return 1;
	}

	srand(time(NULL));

	FILE *html = fopen("sudokubook.html", "w");

	if (html == NULL) {
		perror("error opening file.");
		return 1;
	}

	writehead(html);
	writebody(html, pagecount);

	return 0;
}
