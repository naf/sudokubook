CC = cc
CFLAGS = -Wall
TARGET = sudokubook
INSTALL_DIR = /usr/local/bin

all: $(TARGET)

$(TARGET): sudokubook.c
	$(CC) $(CFLAGS) $< -o $@

install: $(TARGET)
	@printf "installing $(TARGET) to $(INSTALL_DIR)..."
	@cp $(TARGET) $(INSTALL_DIR)/$(TARGET)
	@printf " done.\n"

uninstall:
	@printf "uninstalling $(TARGET) from $(INSTALL_DIR)..."
	@rm -f $(INSTALL_DIR)/$(TARGET)
	@printf " done.\n"

clean:
	@printf "cleaning up..."
	@rm -f $(TARGET)
	@printf " done.\n"

.PHONY: all clean install uninstall
